package de.kxmpetentes.web.panel.core.entity;

import java.util.Locale;
import javax.persistence.Embeddable;

@Embeddable
public class UserSetting {

  private Locale language = Locale.ENGLISH;
  private String hexColorCode = "#94e2d5";
  private String uploaderPageTitle = "Web Panel | Image";
  private String themeName = "Dark";

  public void setLanguage(Locale language) {
    this.language = language;
  }

  public Locale getLanguage() {
    return language;
  }

  public void setHexColorCode(String hexColorCode) {
    this.hexColorCode = hexColorCode;
  }

  public String getHexColorCode() {
    return hexColorCode;
  }

  public void setUploaderPageTitle(String uploaderPageTitle) {
    this.uploaderPageTitle = uploaderPageTitle;
  }

  public String getUploaderPageTitle() {
    return uploaderPageTitle;
  }

  public String getThemeName() {
    return themeName;
  }

  public void setThemeName(String themeName) {
    this.themeName = themeName;
  }
}

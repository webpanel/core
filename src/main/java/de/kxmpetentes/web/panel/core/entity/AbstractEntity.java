package de.kxmpetentes.web.panel.core.entity;

import java.util.UUID;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.Type;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

@MappedSuperclass
public abstract class AbstractEntity {

  @Id
  @GeneratedValue
  @Type(type = "uuid-char")
  private UUID id;

  @NotNull
  public UUID getId() {
    return id;
  }

  @Internal
  public void setId(@NotNull UUID id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    if (id != null) {
      return id.hashCode();
    }
    return super.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof AbstractEntity other)) {
      return false; // null or other class
    }

    if (id != null) {
      return id.equals(other.id);
    }
    return super.equals(other);
  }
}

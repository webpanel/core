package de.kxmpetentes.web.panel.core.service;

import de.kxmpetentes.web.panel.core.entity.User;
import java.util.UUID;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, UUID> {

  @Nullable User findByUsername(String username);

  @Nullable User findByApiToken(UUID apiToken);

}
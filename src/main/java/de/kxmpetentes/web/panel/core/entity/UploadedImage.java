package de.kxmpetentes.web.panel.core.entity;

import java.time.Instant;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

@Entity
@Table(name = "images")
public class UploadedImage extends AbstractEntity {

  @Column(unique = true)
  private String imageId;
  private String uploader;
  private Date uploadTime = Date.from(Instant.now());
  private String hexColor;
  private int views = 0;
  private String domain;
  private String fileName;

  public UploadedImage() {
    //Emptry constructor used for Hibernate
  }

  @NotNull
  public String getImageId() {
    return imageId;
  }

  public void setImageId(@NotNull String name) {
    this.imageId = name;
  }

  @NotNull
  public String getUploader() {
    return uploader;
  }

  public void setUploader(@NotNull String uploader) {
    this.uploader = uploader;
  }

  @NotNull
  public Date getUploadTime() {
    return uploadTime;
  }

  public void setUploadTime(@NotNull Date uploadTime) {
    this.uploadTime = uploadTime;
  }

  @NotNull
  public String getHexColor() {
    return hexColor;
  }

  public void setHexColor(@NotNull String hexColor) {
    this.hexColor = hexColor;
  }

  public int getViews() {
    return views;
  }

  @Internal
  public void setViews(int views) {
    this.views = views;
  }

  public void incrementViews() {
    views++;
  }

  @NotNull
  public String getDomain() {
    return domain;
  }

  public void setDomain(@NotNull String domain) {
    this.domain = domain;
  }

  @NotNull
  public String getFileName() {
    return fileName;
  }

  public void setFileName(@NotNull String fileName) {
    this.fileName = fileName;
  }
}

package de.kxmpetentes.web.panel.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration {

  private static final Map<String, Object> CONFIG_MAP = new HashMap<>();

  static {
    File configFolder = new File("config");
    File file = new File("config", "datasource.properties");
    if (!configFolder.exists()) {
      if (!configFolder.mkdirs()) {
        throw new IllegalStateException("Could not create configuration folder");
      }
    }

    if (!file.exists()) {
      try {
        if (!file.createNewFile()) {
          throw new IllegalStateException("Could not create datasource file");
        }
      } catch (IOException e) {
        throw new IllegalStateException("Could not create datasource file");
      }

      setDefaults();
    }

    Path path = Paths.get(file.toURI());
    List<String> strings;
    try {
      strings = Files.readAllLines(path, StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new IllegalStateException("Could not load config file");
    }

    for (String string : strings) {
      String[] buffer = string.split("=");
      if (buffer.length < 2) {
        continue;
      }

      CONFIG_MAP.put(buffer[0], buffer[1]);
    }

    try {
      DataSourceConfiguration.save(file);
    } catch (IOException e) {
      throw new IllegalStateException("Could not save datasource file");
    }
  }

  private static void setDefaults() {
    CONFIG_MAP.put("hostname", "localhost");
    CONFIG_MAP.put("port", 5432);
    CONFIG_MAP.put("database", "postgres");
    CONFIG_MAP.put("user", "postgres");
    CONFIG_MAP.put("password", "postgres");
  }

  private static void save(File file) throws IOException {
    try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(file.toURI()), StandardCharsets.UTF_8)) {
      for (Map.Entry<String, Object> entry : CONFIG_MAP.entrySet()) {
        writer.write(String.format("%s=%s\n", entry.getKey(), entry.getValue()));
      }
    }
  }

  @Bean
  @Internal
  public DataSource getDataSource() {
    DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
    int port = Integer.parseInt(String.valueOf(CONFIG_MAP.get("port")));
    dataSourceBuilder.url(String.format("jdbc:postgresql://%s:%d/%s", CONFIG_MAP.get("hostname"),
        port, CONFIG_MAP.get("database")));
    dataSourceBuilder.username(String.valueOf(CONFIG_MAP.get("user")));
    dataSourceBuilder.password(String.valueOf(CONFIG_MAP.get("password")));
    return dataSourceBuilder.build();
  }

}

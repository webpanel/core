package de.kxmpetentes.web.panel.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.jetbrains.annotations.NotNull;

@Entity
@Table(name = "shorten_urls")
public class ShortenUrl extends AbstractEntity {

  private String originalUrl;
  @Column(unique = true)
  private String redirectUrl;

  public ShortenUrl() {
    //Emptry constructor used for Hibernate
  }

  public ShortenUrl(@NotNull String originalUrl, @NotNull String redirectUrl) {
    this.originalUrl = originalUrl;
    this.redirectUrl = redirectUrl;
  }

  @NotNull
  public String getOriginalUrl() {
    return originalUrl;
  }

  public void setOriginalUrl(@NotNull String originalUrl) {
    this.originalUrl = originalUrl;
  }

  @NotNull
  public String getRedirectUrl() {
    return redirectUrl;
  }

  public void setRedirectUrl(@NotNull String redirectUrl) {
    this.redirectUrl = redirectUrl;
  }

}

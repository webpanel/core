package de.kxmpetentes.web.panel.core.service;

import de.kxmpetentes.web.panel.core.entity.User;
import java.util.Optional;
import java.util.UUID;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.Blocking;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Blocking
public class UserService {

  private final UserRepository repository;

  @Internal
  @Autowired
  public UserService(UserRepository repository) {
    this.repository = repository;
  }

  @NotNull
  public Optional<User> get(@NotNull UUID id) {
    return repository.findById(id);
  }

  @NotNull
  public User update(@NotNull User entity) {
    return repository.save(entity);
  }

  public void delete(@NotNull UUID id) {
    repository.deleteById(id);
  }

  @NotNull
  public Page<User> list(@NotNull Pageable pageable) {
    return repository.findAll(pageable);
  }

  public int count() {
    return (int) repository.count();
  }

  @Nullable
  public User getByApiToken(@NotNull UUID apiToken) {
    return repository.findByApiToken(apiToken);
  }

  @Nullable
  public User getByName(@NotNull String name) {
    return repository.findByUsername(name);
  }

}

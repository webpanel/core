package de.kxmpetentes.web.panel.core.utils;

public final class Checks {

  private Checks() {
  }

  public static <T> T notNull(T instance, String name) {
    if (instance == null) {
      throw new IllegalArgumentException(name + " may not be null");
    }

    return instance;
  }

}

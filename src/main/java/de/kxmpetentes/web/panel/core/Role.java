package de.kxmpetentes.web.panel.core;

public enum Role {
  USER,
  URL_EDITOR,
  ADMIN
}

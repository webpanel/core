package de.kxmpetentes.web.panel.core.utils;

import java.net.URI;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class RedirectUtil {

  private RedirectUtil() {
  }

  public static ResponseEntity<?> getHomeRedirect(HttpServletRequest request) {
    URI location = URI.create(request.getRequestURL().toString().replace(request.getServletPath(), ""));
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setLocation(location);
    return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY)
        .headers(httpHeaders)
        .build();
  }

}

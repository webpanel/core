package de.kxmpetentes.web.panel.core.service;

import de.kxmpetentes.web.panel.core.entity.ShortenUrl;
import java.util.Optional;
import java.util.UUID;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.Blocking;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Blocking
public class ShortenUrlService {

  private final ShortenUrlRepository repository;

  @Internal
  @Autowired
  public ShortenUrlService(ShortenUrlRepository repository) {
    this.repository = repository;
  }

  @NotNull
  public Optional<ShortenUrl> get(@NotNull UUID id) {
    return repository.findById(id);
  }

  @NotNull
  public ShortenUrl update(@NotNull ShortenUrl entity) {
    return repository.save(entity);
  }

  public void delete(@NotNull UUID id) {
    repository.deleteById(id);
  }

  @NotNull
  public Page<ShortenUrl> list(@NotNull Pageable pageable) {
    return repository.findAll(pageable);
  }

  public int count() {
    return (int) repository.count();
  }

  @Nullable
  public ShortenUrl getByRedirectUrl(@Nullable String redirectUrl) {
    return repository.findShortenUrlByRedirectUrl(redirectUrl);
  }

}

package de.kxmpetentes.web.panel.core.service;

import de.kxmpetentes.web.panel.core.entity.UploadedImage;
import java.util.List;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.Blocking;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Blocking
public class UploadedImageService {

  private final UploadedImageRepository repository;

  @Internal
  @Autowired
  public UploadedImageService(UploadedImageRepository repository) {
    this.repository = repository;
  }

  public void deleteImage(@NotNull UploadedImage uploadedImage) {
    repository.delete(uploadedImage);
  }

  public void insertOrUpdate(@NotNull UploadedImage uploadedImage) {
    repository.save(uploadedImage);
  }

  @NotNull
  public List<UploadedImage> getAllImages() {
    return repository.findAll();
  }

  @Nullable
  public UploadedImage getImageByImageId(@NotNull String name) {
    return repository.findByImageId(name);
  }

  public long count() {
    return repository.count();
  }

  @NotNull
  public List<UploadedImage> getImagesByDomain(@NotNull String domain) {
    return repository.findAllByDomain(domain);
  }

  @NotNull
  public List<UploadedImage> getImagesByUploader(@NotNull String uploader) {
    return repository.findAllByUploaderOrderByUploadTimeDesc(uploader);
  }

  @Nullable
  public UploadedImage getImageByName(@NotNull String name) {
    return repository.findByFileName(name);
  }

  public long countByUploader(@NotNull String uploader) {
    return repository.countAllByUploader(uploader);
  }

  public long deleteAllByUploader(@NotNull String uploader) {
    return repository.deleteAllByUploader(uploader);
  }

}

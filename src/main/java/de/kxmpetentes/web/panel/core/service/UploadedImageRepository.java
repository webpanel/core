package de.kxmpetentes.web.panel.core.service;

import de.kxmpetentes.web.panel.core.entity.UploadedImage;
import java.util.List;
import java.util.UUID;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UploadedImageRepository extends JpaRepository<UploadedImage, UUID> {

  @NotNull List<UploadedImage> findAllByUploaderOrderByUploadTimeDesc(String uploader);

  @NotNull List<UploadedImage> findAllByDomain(String domain);

  @Nullable UploadedImage findByImageId(String name);

  @Nullable UploadedImage findByFileName(String fileName);

  long countAllByUploader(String uploader);

  long deleteAllByUploader(String uploader);

}

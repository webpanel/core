package de.kxmpetentes.web.panel.core.service;

import de.kxmpetentes.web.panel.core.entity.ShortenUrl;
import java.util.UUID;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShortenUrlRepository extends JpaRepository<ShortenUrl, UUID> {

  @Nullable ShortenUrl findShortenUrlByRedirectUrl(String redirectUrl);

}
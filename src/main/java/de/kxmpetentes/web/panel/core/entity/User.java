package de.kxmpetentes.web.panel.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.kxmpetentes.web.panel.core.Role;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

@Entity
@Table(name = "application_user")
public class User extends AbstractEntity {

  @Length(min = 3)
  @Column(unique = true)
  private String username;
  @Length(min = 3)
  @Column(unique = true)
  private String name;
  @JsonIgnore
  private String hashedPassword;
  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.EAGER)
  private Set<Role> roles;
  @GeneratedValue
  @Type(type = "uuid-char")
  @Column(unique = true)
  private UUID apiToken;
  @Embedded
  private UserSetting userSettings = new UserSetting();

  public User() {
    //Emptry constructor used for Hibernate
  }

  public User(@NotNull String username, @NotNull String name, @NotNull String hashedPassword, @NotNull Role... roles) {
    this.username = username;
    this.name = name;
    this.hashedPassword = hashedPassword;
    if (roles.length == 0) {
      this.roles = Set.of(Role.USER);
    } else {
      this.roles = Set.of(roles);
    }
  }

  @NotNull
  public String getUsername() {
    return username;
  }

  public void setUsername(@NotNull String username) {
    this.username = username;
  }

  @NotNull
  public String getName() {
    return name;
  }

  public void setName(@NotNull String name) {
    this.name = name;
  }

  @NotNull
  public String getHashedPassword() {
    return hashedPassword;
  }

  public void setHashedPassword(@NotNull String hashedPassword) {
    this.hashedPassword = hashedPassword;
  }

  @NotNull
  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(@NotNull Set<Role> roles) {
    this.roles = roles;
  }

  public boolean hasRole(@NotNull Role role) {
    if (roles.contains(Role.ADMIN)) {
      return true;
    }

    return roles.contains(role);
  }

  public void addRole(@NotNull Role role) {
    roles.add(role);
  }

  @NotNull
  public UUID getApiToken() {
    return apiToken;
  }

  public void setApiToken(@NotNull UUID apiToken) {
    this.apiToken = apiToken;
  }

  @NotNull
  public UserSetting getUserSettings() {
    return userSettings;
  }

  public void setUserSettings(@NotNull UserSetting userSettings) {
    this.userSettings = userSettings;
  }
}

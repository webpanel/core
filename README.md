### Gradle implementation 
````Groovy
repositories {
    maven { url 'https://repo.mikka.systems/maven-public' }
}

dependencies {
    implementation 'de.kxmpetentes.web.panel:core:1.8.0'
}
````
